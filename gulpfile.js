var gulp = require('gulp'),
    sass = require('gulp-sass'),
    jade = require('gulp-jade'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    smushit = require('gulp-smushit'),
    browserSync = require('browser-sync'),
    shell = require('gulp-shell');

gulp.task('browser-sync', ['scss'],  function() {
  browserSync.init({
    server: {
      baseDir: "./app/"
    }
  });
});

gulp.task('scss', function(){
  return gulp.src('./src/scss/**/*.scss')
    .pipe(sass({
      includePaths: ['./src/scss/', './src/scss/blocks/', './src/scss/mixins/'],
      errLogToConsole: true
    }))
    // .pipe(cssmin())
    .pipe(gulp.dest('./app/css/'))
    .pipe(browserSync.reload({stream:true}))
});

// Собираем Jade
gulp.task('jade', function() {
    gulp.src('./src/*.jade')
        .pipe(jade({
            pretty: true
        }))
        .on('error', console.log)
    .pipe(gulp.dest('./app/'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('reload', function () {
  browserSync.reload();
});

gulp.task('js', function () {
    gulp.src("src/js/**/*.js")
      // .pipe(uglify())
      .pipe(gulp.dest('app/js/'))
      .pipe(browserSync.reload({stream:true}))
});

gulp.task('imagemin', function () {
    return gulp.src('src/images/**/*')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))
        // .pipe(smushit({
        //     verbose: true
        // }))
        .pipe(gulp.dest('app/images/'))
        .pipe(browserSync.reload({stream:true}))
});

gulp.task('watch', function(){
  gulp.watch('./src/*.jade', ['jade']);
  gulp.watch('./src/scss/**/*.scss', ['scss']);
  gulp.watch('src/js/**/*.js', ['js']);
  gulp.watch('src/images/**/*', ['imagemin']);
  // gulp.watch('**/*.{php,inc,info,css}',['reload']);
});

gulp.task('default', ['browser-sync', 'watch']);
